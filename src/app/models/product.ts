export class ProductViewModel {
    id: string;
    sku: string;
    upc: string;
    pname: string;
    taxcode: string;
    userid: string;
}