import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './components/guards/auth-guard';
import { SinUpComponent } from './components/sin-up/sin-up/sin-up.component';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full'
  },
  { path: 'sin-up', component: SinUpComponent},
  {
    path: 'dashboard', canActivate: [AuthGuard] ,loadChildren: () => import('../app/modules/inner.module').then(m => m.InnerModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
