import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { ProductViewModel } from '../models/product';
import { Credentials } from '../models/credentials';
import { Response, RequestOptions, Http, Headers } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Member } from '../models/member';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  filesToUpload: Array<File>;

  private BASE_URL = "http://localhost:8089/";

  // tslint:disable-next-line:member-ordering
  // headers: HttpHeaders;
  headers: Headers;
  // tslint:disable-next-line:member-ordering
  options: RequestOptions;

  constructor(
    private http: HttpClient,
    private oldHttp: Http,
    private router: Router
  ) {
    this.filesToUpload = [];
   }

   upload(id: string) {
    this.makeFileRequest("http://localhost:8089/product/image?id="+id, [], this.filesToUpload).then((result) => {
      console.log(result);
    }, (error) => {
      console.log(error);
    });
  }

   fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>> fileInput.target.files;
    }

   makeFileRequest(url:string, params:Array<string>, files:Array<File>) {
    return new Promise((resolve, reject) => {
      var formData:any = new FormData();
      var xhr = new XMLHttpRequest();
      for(var i=0; i<files.length;i++) {
        formData.append('uploads[]', files[i], files[i].name);
      }
      xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) {
          if(xhr.status==200) {
            console.log('image uploaded successfully!');
          } else {
            reject(xhr.response);
          }
        }
      }
  
      xhr.open("POST", url, true);
      xhr.setRequestHeader("x-auth-token", localStorage.getItem("xAuthToken"));
      xhr.send(formData);
    });
  }

  getCurrentUserId() {
    const currentUser = JSON.parse(localStorage.getItem('JWT_Token'));
    return currentUser.userId + '';
  }

  saveProduct(product: ProductViewModel): Observable<any> {
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    console.log(product.userid);
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.post('http://localhost:8089/' + 'product/add', product, { headers: headerss })
      .pipe(
        map(this.extractData),
        catchError(this.handleApiError)
      );
  }

  getAllProducts(): Observable<ProductViewModel[]> {
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })

    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.get(this.BASE_URL + 'product/all/' + this.getCurrentUserId(), this.options)
      .pipe(
        map((resp: Response) => resp.json()),
        catchError(this.handleApiError)
      )
  }

  login(credentials: Credentials): Observable<any> {
    console.log(credentials);
    const url = this.BASE_URL + 'token';
    const encodedCredentials = btoa(credentials.username + ':' + credentials.password);
    const basicHeader = 'Basic ' + encodedCredentials;
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //     'Authorization': basicHeader
    //   })
    // }
    const headerss = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': basicHeader
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.get(url, this.options)
      .pipe(
        map(this.extractData),
        catchError(this.handleApiError)
      );
  }

  register(mebr: Member): Observable<any> {
    const body = JSON.stringify(mebr);
    const basicHeader = 'Basic ' + '';
    const headerss = new Headers({
      // 'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      // 'X-Content-Type-Options': 'nosniff'
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.post('http://localhost:8089/' + 'registration', body, { headers: headerss })
      .pipe(
        map(this.extractData),
        catchError(this.handleApiError)
      );
  }

  updatUser(mebr: Member): Observable<any> {
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    const body = JSON.stringify(mebr);
    return this.oldHttp.post('http://localhost:8089/product/updateUser/' + this.getCurrentUserId(), body, this.options)
      .pipe(
        map((resp: Response) => console.log('done')),
        catchError(this.handleApiError)
      );
  }

  updatProduct(prod: ProductViewModel): Observable<any> {
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.post('http://localhost:8089/' + 'product/update', prod, { headers: headerss })
      .pipe(
        map((resp: Response) => console.log('done')),
        catchError(this.handleApiError)
      );
  }

  deleteProduct(id: string): Observable<any>{
    console.log(id);
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.delete('http://localhost:8089/' + 'product/delete/' + id, this.options)
      .pipe(
        map(this.extractData),
        catchError(this.handleApiError)
      );
  }

  getOneProduct(upc: string): Observable<any>{
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.get('http://localhost:8089/' + 'product/product/' + upc, this.options)
    .pipe(map(this.extractData),
    catchError(this.handleApiError)
    );
  }

  getUser(): Observable<any>{
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    return this.oldHttp.get(this.BASE_URL + 'product/product/user/' + this.getCurrentUserId(), this.options)
      .pipe(
        map((resp: Response) => resp.json()),
        catchError(this.handleApiError)
      )
  }

  downloadFile(){
    const currentSession = JSON.parse(localStorage.getItem('JWT_Token'));
    const headerss = new Headers({
      'Content-Type': 'application/json',
      'Authorization': currentSession.token
    })
    this.options = new RequestOptions({ headers: headerss });
    console.log("hano");
    return this.oldHttp.get(this.BASE_URL + 'product/download/', this.options)
      .pipe(
        map((resp: Response) => resp.json()),
        catchError(this.handleApiError)
      )
  }

  private extractData(res: Response) {
    const body = JSON.parse(JSON.stringify(res || null));
    return body || {};
  }

  setLoginUser(userData: string) {
    localStorage.setItem('JWT_Token', userData);
  }

  setToken(token: string) {
    localStorage.setItem('Session_user', token);
  }

  getCurrentUser() {
    const currentUser = JSON.parse(localStorage.getItem('JWT_Token'));
    return currentUser.lastName + ' ' + currentUser.firstName;
  }

  getToken() {
    return localStorage.getItem('JWT_Token');
  }

  isLogin() {
    return this.getToken() !== null;
  }

  logout() {
    localStorage.removeItem('JWT_Token');
    localStorage.removeItem('Session_user');
    this.router.navigate(['/']);
  }

  private handleApiError(error: any) {
    if (error.status > 0) {
      if (error.status === 500) {
        return throwError('something has gone wrong on the server ask your administrator');
      } else if (error.status === 401) {
        if (error._body === '') {
          return throwError('Unauthorized User');
        } else { 
          return throwError(error._body || 'Server error'); 
        }
      } else {
        return throwError(error._body || 'Server error');
      }
    } else {
      return throwError('Please check your connection');
    }
  }
}
