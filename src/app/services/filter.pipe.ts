import { Pipe, PipeTransform } from '@angular/core';
import { ProductViewModel } from '../models/product';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(products: ProductViewModel[], text: string): ProductViewModel[] {
    if(text == null || text === ""){
      return products;
    }
    return products.filter(pr => pr.upc.includes(text) || pr.pname.includes(text));
  }

}
