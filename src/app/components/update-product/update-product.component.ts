import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductViewModel } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {


  public product: ProductViewModel;
  successMessage: string;
  errorMessage: string;

  constructor(private route: ActivatedRoute, private producctService: ProductService) {
    this.product = new ProductViewModel();
    this.route.params.subscribe((data) => {
      
      this.producctService.getOneProduct(data.upc).subscribe((prod) => {
        console.log(prod);
        this.product = JSON.parse(prod._body);
      });
    });
   }

  ngOnInit() {
   
  }

  updateProduct(form: NgForm){
    this.errorMessage = '';
    this.successMessage = '';
    this.producctService.updatProduct(this.product).subscribe(
      data => {
        this.successMessage = 'Product updated well';
        form.resetForm();
        console.log("product updated well!!");
      },
      error => { this.errorMessage = error + ''; }
    );
  }


}
