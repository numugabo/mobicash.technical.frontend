import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { Observable } from 'rxjs';



@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private productService: ProductService, private router: Router) {}

    canActivate(
        next: ActivatedRouteSnapshot, state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.productService.isLogin()) {
            return true;
        } else {
            this.router.navigate(['/']);
            return false;
        }
    }
}