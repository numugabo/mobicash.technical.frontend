import { Component, OnInit } from '@angular/core';
import { Member } from 'src/app/models/member';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-sin-up',
  templateUrl: './sin-up.component.html',
  styleUrls: ['./sin-up.component.css']
})
export class SinUpComponent implements OnInit {
  member: Member;
  retypePassword: string;
  successMessage: string;
  errorMessage: string;
  constructor(
    private router: Router,
    private productService: ProductService
  ) {
    this.member = new Member();
  }

  ngOnInit() {
  }

  cancel() {
    this.router.navigate(['/'])
  }

  onSubmit(form: NgForm) {
    this.errorMessage = '';
    this.successMessage = '';
    if (form.invalid) {
      return;
    } if (this.member.password !== this.retypePassword) {
      this.errorMessage = 'Password are not matched';
    } else {
      this.productService.register(this.member)
        .subscribe(
          data => {
            this.successMessage = 'User well registered';
            form.resetForm();
          },
          error => { this.errorMessage = error + ''; }
        )
    }
  }

}
