import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Credentials } from 'src/app/models/credentials';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: Credentials;
  mToken: string

  constructor(private router: Router, private productService: ProductService) {
    this.credentials = new Credentials();
  }

  ngOnInit() {
  }

  onLogin(loginForm: NgForm) {
    this.credentials = Object.assign({}, loginForm.value);
    this.credentials.token = this.mToken;
    this.productService.login(this.credentials).subscribe((response) => {
      if (response.ok === true) {
        loginForm.reset();
        const loginResponseData = JSON.parse(response._body);
        this.productService.setToken(loginResponseData.token.token);
        this.productService.setLoginUser(JSON.stringify(loginResponseData));
        this.router.navigate(['/dashboard']);
      } else {
        loginForm.reset();
      }
    },
      (err) => {
        console.log(err + "")
      }
    );
  }


  sinUp() {
    this.router.navigate(['/sin-up']);
  }

}
