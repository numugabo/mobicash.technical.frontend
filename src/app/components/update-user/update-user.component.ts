import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Member } from 'src/app/models/member';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  member: Member;
  successMessage: string;
  errorMessage: string;

  constructor(private productService: ProductService, private route: ActivatedRoute) {
    this.member = new Member();
   }

  ngOnInit() {
      this.productService.getUser().subscribe((us) => {
        this.member.firstname = us.firstName;
        this.member.lastname = us.lastName;
        this.member.email = us.username;
      });
  }

  updateUser(form: NgForm){
    this.errorMessage = '';
    this.successMessage = '';
    
    this.productService.updatUser(this.member).subscribe(
      data => {
        this.successMessage = 'User well updated';
        form.resetForm();
      },
      error => { this.errorMessage = error + ''; }
    );
  }
}
