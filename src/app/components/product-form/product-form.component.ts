import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { NgForm } from '@angular/forms';
import { ProductViewModel } from 'src/app/models/product';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  private products : ProductViewModel[] = [];
  private product: ProductViewModel;
  private selectedFile: File = null;
  successMessage: string;
  errorMessage: string;

  constructor(private productService: ProductService) { 
    this.product = new ProductViewModel();
  }

  ngOnInit() {
  }

  createProduct(form: NgForm){
    this.errorMessage = '';
    this.successMessage = '';
    this.product.userid = this.productService.getCurrentUserId();
    this.productService.saveProduct(this.product).subscribe(
      data => {
        this.productService.upload(JSON.stringify(JSON.parse(data._body).id));
        this.successMessage = 'Product recorded well';
        form.resetForm();
        this.products.push(this.product);
        console.log("product added!!");
      },
      error => { this.errorMessage = error + ''; }
    );
  }

}
