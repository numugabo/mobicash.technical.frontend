import { Component, OnInit } from '@angular/core';
import { ProductViewModel } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private productService: ProductService) {
   }

  ngOnInit() {
  }

  logout() {
    this.productService.logout();
  }

}
