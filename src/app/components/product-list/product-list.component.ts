import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ProductViewModel } from 'src/app/models/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  private products : ProductViewModel[] = [];
  private searchText: string;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getAllProducts();
    console.log(this.getAllProducts());
  }

  getAllProducts(){
    this.productService.getAllProducts().subscribe(
      all =>{
        this.products = all;
      }
    );
  }

  deleteProduct(product: any){
    console.log(product.id);
      this.productService.deleteProduct(product.id).subscribe(
        res => {
          let indexOfProduct = this.products.indexOf(product);
          this.products.splice(indexOfProduct, 1);
        }
      );
    }

    downloadFile(){
      this.productService.downloadFile().subscribe(
        res => {
          console.log('downloaded');
        }
      );
    }

}
