import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { CommonModule } from '@angular/common';
import {ProductFormComponent } from '../components/product-form/product-form.component'
import { FormsModule } from '@angular/forms';
import { ProductListComponent } from '../components/product-list/product-list.component';
import { FilterPipe } from '../services/filter.pipe'
import { UpdateProductComponent } from '../components/update-product/update-product.component';
import { UpdateUserComponent } from '../components/update-user/update-user.component';




@NgModule({
    declarations: [DashboardComponent,
         ProductFormComponent,
          ProductListComponent,
           FilterPipe, UpdateUserComponent,
           UpdateProductComponent],
    providers: [],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '', component: DashboardComponent,
                children: [
                    {
                        path: 'product', component: ProductFormComponent
                    },
                    {
                        path: 'products', component: ProductListComponent
                    },
                    {
                        path: 'edit', component: UpdateUserComponent
                    },
                    { path: 'update-sku/:upc', component: UpdateProductComponent }
                ]
            },
        ]


        )],
})
export class InnerModule { }